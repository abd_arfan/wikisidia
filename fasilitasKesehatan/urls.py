from django.contrib import admin
from django.urls import path
from .views import *

urlpatterns = [
    path('Faskes/', listFaskes, name="listFaskes"),
    path('Faskes/CreateFaskes/', createFaskes, name="createFaskes"),
    path('deleteFaskes/<str:kode_faskes_nasional>', deleteFaskes, name="deleteFaskes"),
    path('updateFaskes/<str:kode_faskes_nasional>', updateFaskes, name="updateFaskes"),
    path('getLokasi/<str:id_lokasi>', getLokasi, name="getLokasi"),
    path('getTipeFaskes/<str:tipe>', getTipeFaskes, name="getTipeFaskes")
]