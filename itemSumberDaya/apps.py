from django.apps import AppConfig


class ItemsumberdayaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'itemSumberDaya'
