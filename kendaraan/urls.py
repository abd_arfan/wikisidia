from django.contrib import admin
from django.urls import path
from .views import *

urlpatterns = [
    path('Kendaraan/', listKendaraan, name="listKendaraan"),
    path('deleteKendaraan/<str:nomor>', deleteKendaraan, name="deleteKendaraan"),
    path('Kendaraan/CreateKendaraan/', createKendaraan, name="createKendaraan"),
    path('updateKendaraan/<str:nomor_lama>', updateKendaraan, name="updateKendaraan")
]