from django.contrib import admin
from django.urls import path
from .views import *

urlpatterns = [
    path('Lokasi/', listLokasi, name="lokasi"),
    path('Lokasi/CreateLokasi/', createLokasi, name="createLokasi"),
    path('updateLokasi/<str:id_lokasi>', updateLokasi, name="updateLokasi")

]