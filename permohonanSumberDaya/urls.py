from django.contrib import admin
from django.urls import path
from .views import buatPermohonan, buatRiwayatStatus, createPermohonanSumberDaya, listPermohonanSD, deletePermohonanSD, detailPermohonan, riwayatStatus, updatePermohonan

urlpatterns = [
    path('PermohonanSumberDaya/', listPermohonanSD, name='listPermohonanSD'),
    path('PermohonanSumberDaya/buatPermohonan', createPermohonanSumberDaya, name='createPermohonanSumberDaya'),
    path('deletePermohonanSD/<str:no_transaksi_sumber_daya>', deletePermohonanSD, name="deletePermohonanSD"),
    path('detailPermohonan/<str:no_transaksi_sumber_daya>/<str:kode_sp>', detailPermohonan, name='detailPermohonan'),
    path('updatePermohonan/<str:no_transaksi_sumber_daya>/<str:nama>/<str:no_urut>/<str:kode>', updatePermohonan, name="updatePermohonan"),
    path('riwayatStatus/<str:no_transaksi_sumber_daya>', riwayatStatus, name="riwayatStatus"),
    path('buatRiwayatStatus/<str:no_transaksi_sumber_daya>/<str:kode_sp>/<str:command>', buatRiwayatStatus, name='buatRiwayatStatus')
]