from django import forms

class createPesananForms(forms.Form):
    jumlah = forms.CharField(label='Jumlah Item', max_length=10, widget=forms.TextInput(attrs = {
        'class':'form-control',
        'type':'text',
        'required':True,
        'placeholder':'Masukkan Jumlah Item',        
        'oninvalid' : "alert('Terdapat data yang belum diisi, silahkan lengkapi data terlebih dahulu! 🙏🏻😊🙏🏻')",
    }))