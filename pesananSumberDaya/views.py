from datetime import datetime
from django.shortcuts import render, redirect
from django.db import connection
from collections import namedtuple
from django.http import JsonResponse
from .forms import createPesananForms

# Create your views here.

# general function
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def get_item_from_supplier(request, username_supplier):
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select kode from item_sumber_daya where username_supplier = '" + username_supplier + "'")
    hasil = namedtuplefetchall(cursor)
    argument = {
        'table' : hasil,
    }

    return JsonResponse(hasil, safe=False)

def createPesananSD(request):
    message = ""
    tanggal = str(datetime.now().date())
    cursor = connection.cursor()
    role_admin_satgas = False

    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
    except:
        return redirect('/')

    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select nomor from transaksi_sumber_daya order by nomor desc")
    nomor_transaksi = namedtuplefetchall(cursor)[0]
    if (len(nomor_transaksi) == 0):
        no_transaksi = 1
    else:
        no_transaksi = nomor_transaksi.nomor+1
    
    nomor = str(no_transaksi)
    
    cursor.execute("select distinct p.nama from pengguna p, admin_satgas adm where p.username=adm.username and adm.username='" + username + "'")
    nama_petugas = namedtuplefetchall(cursor)[0].nama

    cursor.execute("select nama_organisasi, username from supplier order by nama_organisasi")
    supplier_view = namedtuplefetchall(cursor)

    query = "select no_urut from daftar_item di, pesanan_sumber_daya psd, transaksi_sumber_daya tsd"
    query += " where di.no_transaksi_sumber_daya=tsd.nomor and tsd.nomor=psd.nomor_pesanan and psd.nomor_pesanan='" + nomor + "'"
    query += " order by no_urut desc"
    cursor.execute(query)
    no_urut = namedtuplefetchall(cursor)
    if (len(no_urut) == 0):
        no_urut = 1
    else :
        no_urut = no_urut[0].no_urut + 1
    
    cursor.execute("select nama, kode from item_sumber_daya order by kode")
    kode_item_view = namedtuplefetchall(cursor)

    query = "select di.no_urut, isd.nama, isd.harga_satuan, di.jumlah_item"
    query += " from transaksi_sumber_daya tsd, daftar_item di, item_sumber_daya isd, pesanan_sumber_daya psd"
    query += " where tsd.nomor=di.no_transaksi_sumber_daya and di.kode_item_sumber_daya=isd.kode and tsd.nomor=psd.nomor_pesanan"
    query += " and tsd.nomor='" + nomor + "' order by di.no_urut"
    cursor.execute(query)
    daftar_item = namedtuplefetchall(cursor)

    if request.method == 'POST':
        kode_item = request.POST['kode_item']
        supplier = request.POST['supplier']
        jumlah = get_jumlah(request)[0]
        try:
            cursor.execute("insert into transaksi_sumber_daya values('" + nomor + "', '" + tanggal + "')")
            cursor.execute("insert into daftar_item values('" + nomor + "', '" + str(no_urut) + "', '" + jumlah + "', '" + kode_item + "')")
            cursor.execute("select no_transaksi_sumber_daya, max(harga_kumulatif) as max_harga from daftar_item where no_transaksi_sumber_daya = '" + nomor + "' group by no_transaksi_sumber_daya")
            harga = namedtuplefetchall(cursor)[0]
            harga = str(harga.max_harga)
            cursor.execute("insert into pesanan_sumber_daya values('" + nomor + "', '" + username + "', '" + harga + "')")
            cursor.execute("insert into riwayat_status_pesanan values('REQ-SUP', '" + nomor + "', '" + supplier + "', '" + tanggal + "')")
            
            cursor.close()
            url = '/updatePesanan/TOR' + nomor
            return redirect(url)

        except Exception as e:
            message = str(e)[80:]
        
    cursor.close()
    form = createPesananForms()

    argument = {
        'form': form,
        'no_transaksi': no_transaksi,
        'nama_petugas': nama_petugas,
        'role_admin_satgas' : role_admin_satgas,
        'supplier': supplier_view,
        'no_urut': no_urut,
        'kode_item': kode_item_view,
        'message': message,
        'daftar_item': daftar_item
    }

    return render(request, "createPesananSD.html", argument)

# read list pesanan_sumber_daya
def listPesananSD(request):
    cursor = connection.cursor()
    role_admin_satgas = False
    role_supplier = False

    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
        if (request.session['role'] == 'supplier'):
            role_supplier = True
    except:
        return redirect('/')

    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    if(request.session['role'] == 'admin_satgas') :
        cursor.execute("select distinct tsd.nomor, rsp.no_pesanan, p.nama, tsd.tanggal, tsd.total_berat, tsd.total_item, psd.total_harga, rsp.kode_status_pesanan, rsp.tanggal as tanggal_riwayat from transaksi_sumber_daya tsd, pesanan_sumber_daya psd, riwayat_status_pesanan rsp, pengguna p, admin_satgas adm where tsd.nomor=psd.nomor_pesanan and rsp.no_pesanan=psd.nomor_pesanan and psd.username_admin_satgas=adm.username and adm.username=p.username and (rsp.no_pesanan, rsp.tanggal) in (select rsp.no_pesanan, max(rsp.tanggal) as tanggal_riwayat from riwayat_status_pesanan rsp group by rsp.no_pesanan) and username_admin_satgas = '" + username + "' group by tsd.nomor, rsp.no_pesanan, p.nama, tsd.tanggal, tsd.total_berat, tsd.total_item, psd.total_harga, rsp.kode_status_pesanan order by tsd.nomor")
    if (request.session['role'] == 'supplier') :
        cursor.execute("select distinct tsd.nomor, rsp.no_pesanan, p.nama, tsd.tanggal, tsd.total_berat, tsd.total_item, psd.total_harga, rsp.kode_status_pesanan, rsp.tanggal as tanggal_riwayat from transaksi_sumber_daya tsd, pesanan_sumber_daya psd, riwayat_status_pesanan rsp, pengguna p, admin_satgas adm where tsd.nomor=psd.nomor_pesanan and rsp.no_pesanan=psd.nomor_pesanan and psd.username_admin_satgas=adm.username and adm.username=p.username and (rsp.no_pesanan, rsp.tanggal) in (select rsp.no_pesanan, max(rsp.tanggal) as tanggal_riwayat from riwayat_status_pesanan rsp group by rsp.no_pesanan) and username_supplier= '" + username + "' group by tsd.nomor, rsp.no_pesanan, p.nama, tsd.tanggal, tsd.total_berat, tsd.total_item, psd.total_harga, rsp.kode_status_pesanan order by tsd.nomor")
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    argument = {
        'table' : hasil,
        'role_admin_satgas' : role_admin_satgas,
        'role_supplier' : role_supplier,
    }
    return render(request, "listPesananSD.html", argument)

def detailPesananSD(request, nomor):
    nomor = str(nomor)
    cursor = connection.cursor()
    role_supplier = False
    role_admin_satgas = False

    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
        if (request.session['role'] == 'supplier'):
            role_supplier = True
    except:
        return redirect('/')

    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    if role_admin_satgas:
        cursor.execute("select distinct nomor, nama, tsd.tanggal, total_berat, total_item, total_harga, nama_organisasi from transaksi_sumber_daya tsd, pesanan_sumber_daya, riwayat_status_pesanan rsp, supplier s, pengguna p, admin_satgas adm where nomor=nomor_pesanan and no_pesanan=nomor_pesanan and s.username=rsp.username_supplier and username_admin_satgas=adm.username and adm.username=p.username and nomor = '" + nomor + "' and username_admin_satgas= '" + username + "'")
    elif role_supplier:
        cursor.execute("select distinct nomor, nama, tsd.tanggal, total_berat, total_item, total_harga, nama_organisasi from transaksi_sumber_daya tsd, pesanan_sumber_daya, riwayat_status_pesanan rsp, supplier s, pengguna p, admin_satgas adm where nomor=nomor_pesanan and no_pesanan=nomor_pesanan and s.username=rsp.username_supplier and username_admin_satgas=adm.username and adm.username=p.username and nomor = '" + nomor + "'")

    hasil = namedtuplefetchall(cursor)
    cursor.execute("select di.no_urut, isd.nama, isd.harga_satuan, di.jumlah_item from transaksi_sumber_daya tsd, daftar_item di, item_sumber_daya isd, pesanan_sumber_daya psd where tsd.nomor=di.no_transaksi_sumber_daya and di.kode_item_sumber_daya=isd.kode and tsd.nomor=psd.nomor_pesanan and tsd.nomor='" + nomor + "' order by di.no_urut")
    daftar_item = namedtuplefetchall(cursor)
    cursor.close()

    argument = {
        'table' : hasil,
        'daftar_item': daftar_item,
        'role_admin_satgas' : role_admin_satgas,
        'role_supplier' : role_supplier,
    }
    return render(request, "detailPesananSD.html", argument)

def deletePesananSD(request, nomor):
    nomor = str(nomor)
    cursor = connection.cursor()
    role_admin_satgas = False

    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
    except:
        return redirect('/')
    
    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    cursor.execute("delete from daftar_item where no_transaksi_sumber_daya='" + nomor + "'")
    cursor.execute("delete from pesanan_sumber_daya where nomor_pesanan= '" + nomor + "'")
    cursor.execute("delete from riwayat_status_pesanan rsp where rsp.kode_status_pesanan='REQ-SUP' and rsp.no_pesanan='" + nomor + "'")
    cursor.execute("delete from transaksi_sumber_daya where nomor ='" + nomor + "'")
    cursor.execute("select distinct tsd.nomor, rsp.no_pesanan, p.nama, tsd.tanggal, tsd.total_berat, tsd.total_item, psd.total_harga, rsp.kode_status_pesanan, rsp.tanggal as tanggal_riwayat from transaksi_sumber_daya tsd, pesanan_sumber_daya psd, riwayat_status_pesanan rsp, pengguna p, admin_satgas adm where tsd.nomor=psd.nomor_pesanan and rsp.no_pesanan=psd.nomor_pesanan and psd.username_admin_satgas=adm.username and adm.username=p.username and (rsp.no_pesanan, rsp.tanggal) in (select rsp.no_pesanan, max(rsp.tanggal) as tanggal_riwayat from riwayat_status_pesanan rsp group by rsp.no_pesanan) and username_admin_satgas = '" + username + "' group by tsd.nomor, rsp.no_pesanan, p.nama, tsd.tanggal, tsd.total_berat, tsd.total_item, psd.total_harga, rsp.kode_status_pesanan order by tsd.nomor")
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    message = "Data berhasil dihapus"

    argument = {
        'table' : hasil,
        'role_admin_satgas' : role_admin_satgas,
        'message' : message
    }
    return render(request, "listPesananSD.html", argument)

def get_jumlah(request):
    input = request.POST
    jumlah = input['jumlah']
    return [jumlah]

def get_nama_item(request, kode_item):
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select nama from item_sumber_daya where kode = '" + kode_item + "'")
    hasil = namedtuplefetchall(cursor)
    argument = {
        'table' : hasil,
    }

def updatePesananSD(request, nomor):
    message = ""
    tanggal = str(datetime.now().date())
    nomor = str(nomor)
    cursor = connection.cursor()
    role_admin_satgas = False

    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
    except:
        return redirect('/')

    cursor.execute("set search_path to wikisidia")
    nomor_admin_supplier = "select distinct p.nama, s.nama_organisasi, s.username"
    nomor_admin_supplier += " from pesanan_sumber_daya psd, riwayat_status_pesanan rsp, admin_satgas adm, supplier s, pengguna p"
    nomor_admin_supplier += " where psd.nomor_pesanan=rsp.no_pesanan and adm.username=p.username and rsp.username_supplier=s.username and psd.username_admin_satgas=adm.username and psd.nomor_pesanan='" + nomor + "'"
    cursor.execute(nomor_admin_supplier)
    hasil = namedtuplefetchall(cursor)[0]
    nama_petugas = hasil.nama
    supplier = hasil.nama_organisasi
    username_supplier = hasil.username

    query = "select no_urut from daftar_item di, pesanan_sumber_daya psd, transaksi_sumber_daya tsd"
    query += " where di.no_transaksi_sumber_daya=tsd.nomor and tsd.nomor=psd.nomor_pesanan and psd.nomor_pesanan='" + nomor + "'"
    query += " order by no_urut desc"
    cursor.execute(query)
    no_urut = namedtuplefetchall(cursor)
    if (len(no_urut) == 0):
        no_urut = 1
    else :
        no_urut = no_urut[0].no_urut + 1
    
    cursor.execute("select kode, nama from item_sumber_daya where username_supplier='" + username_supplier + "' order by kode")
    kode_item_view = namedtuplefetchall(cursor)

    query = "select di.no_urut, isd.nama, isd.harga_satuan, di.jumlah_item"
    query += " from transaksi_sumber_daya tsd, daftar_item di, item_sumber_daya isd, pesanan_sumber_daya psd"
    query += " where tsd.nomor=di.no_transaksi_sumber_daya and di.kode_item_sumber_daya=isd.kode and tsd.nomor=psd.nomor_pesanan"
    query += " and tsd.nomor='" + nomor + "' order by di.no_urut"
    cursor.execute(query)
    daftar_item = namedtuplefetchall(cursor)

    if request.method == 'POST':
        kode_item = request.POST['kode_item']
        jumlah = get_jumlah(request)[0]
        try:
            cursor.execute("insert into daftar_item values('" + nomor + "', '" + str(no_urut) + "', '" + jumlah + "', '" + kode_item + "')")
            cursor.execute("update transaksi_sumber_daya set tanggal='" + tanggal + "' where nomor='" + nomor + "'")
            cursor.execute("select no_transaksi_sumber_daya, max(harga_kumulatif) as max_harga from daftar_item where no_transaksi_sumber_daya = '" + nomor + "' group by no_transaksi_sumber_daya")
            harga = namedtuplefetchall(cursor)[0]
            harga = str(harga.max_harga)
            cursor.execute("update pesanan_sumber_daya set total_harga='" + harga + "' where nomor_pesanan='" + nomor + "'")
            cursor.close()
            
        except Exception as e:
            message = str(e)

    cursor.close()
    form = createPesananForms()
    argument = {
        'form': form,
        'nomor': nomor,
        'nama_petugas': nama_petugas,
        'supplier': supplier,
        'no_urut': no_urut,
        'kode_item': kode_item_view,
        'daftar_item': daftar_item,
        'message': message,
        'role_admin_satgas' : role_admin_satgas,
        'nomor_admin_supplier': nomor_admin_supplier,
    }
    return render(request, "updatePesananSD.html", argument)

def deleteItemDiPesanan(request, nomor, no_urut):
    message = ""
    nomor = str(nomor)
    no_urut = str(no_urut)
    cursor = connection.cursor()
    role_admin_satgas = False

    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
    except:
        return redirect('/')
    
    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    cursor.execute("delete from daftar_item where no_urut='" + no_urut + "' and no_transaksi_sumber_daya='" + nomor + "'")
    cursor.execute("select * from daftar_item where no_urut>'" + nomor + "' and no_transaksi_sumber_daya='" + nomor + "' order by no_urut asc")
    daftar_item = namedtuplefetchall(cursor)
    for item in daftar_item:
        no_urut = item.no_urut
        cursor.execute("update daftar_item set no_urut=no_urut-1 where no_transaksi_sumber_daya='" + nomor + "' and no_urut='" + no_urut + "'")

    cursor.close()

    argument = {
        'nomor': nomor,
        'no_urut': no_urut,
        'daftar_item': daftar_item,
        'message': message,
        'role_admin_satgas' : role_admin_satgas,
    }
    return redirect("/updatePesanan/TOR"+nomor)

def riwayatPesananSD(request, nomor):
    nomor = str(nomor)
    cursor = connection.cursor()
    role_supplier = False
    role_admin_satgas = False

    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
        if (request.session['role'] == 'supplier'):
            role_supplier = True
    except:
        return redirect('/')

    cursor.execute("set search_path to wikisidia")
    cursor.execute("select rsp.no_pesanan, rsp.kode_status_pesanan, sp.nama, rsp.username_supplier, rsp.tanggal from riwayat_status_pesanan rsp, status_pesanan sp where rsp.kode_status_pesanan=sp.kode and rsp.no_pesanan = '" + nomor + "' order by tanggal DESC")

    hasil=namedtuplefetchall(cursor)
    cursor.close()
    argument = {
        'table' : hasil,
        'role_admin_satgas' : role_admin_satgas,
        'role_supplier' : role_supplier,
        'nomor': nomor
    }

    return render(request, "riwayatPesananSD.html", argument)

def rejectPesananSD(request, nomor):
    nomor = str(nomor)
    tanggal = str(datetime.now().date())
    cursor = connection.cursor()
    role_supplier = False

    try:
        if (request.session['role'] == 'supplier'):
            role_supplier = True
    except:
        return redirect('/')

    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select distinct username_supplier from riwayat_status_pesanan where no_pesanan='" + nomor + "'")
    supplier = namedtuplefetchall(cursor)[0]
    username_supplier = supplier.username_supplier

    cursor.execute("insert into riwayat_status_pesanan values ('REJ-SUP','" + nomor + "', '" + username_supplier +"', '" + tanggal + "')")
    cursor.execute("select distinct tsd.nomor, rsp.no_pesanan, p.nama, tsd.tanggal, tsd.total_berat, tsd.total_item, psd.total_harga, rsp.kode_status_pesanan, rsp.tanggal as tanggal_riwayat from transaksi_sumber_daya tsd, pesanan_sumber_daya psd, riwayat_status_pesanan rsp, pengguna p, admin_satgas adm where tsd.nomor=psd.nomor_pesanan and rsp.no_pesanan=psd.nomor_pesanan and psd.username_admin_satgas=adm.username and adm.username=p.username and (rsp.no_pesanan, rsp.tanggal) in (select rsp.no_pesanan, max(rsp.tanggal) as tanggal_riwayat from riwayat_status_pesanan rsp group by rsp.no_pesanan) and username_supplier= '" + username + "' group by tsd.nomor, rsp.no_pesanan, p.nama, tsd.tanggal, tsd.total_berat, tsd.total_item, psd.total_harga, rsp.kode_status_pesanan order by tsd.nomor")
    hasil=namedtuplefetchall(cursor)
    cursor.close()

    message = "Pesanan berhasil ditolak"

    argument = {
        'table' : hasil,
        'role_supplier' : role_supplier,
        'message' : message,
    }
    return render(request, "listPesananSD.html", argument)

def processPesananSD(request, nomor):
    nomor = str(nomor)
    tanggal = str(datetime.now().date())
    cursor = connection.cursor()
    role_supplier = False

    try:
        if (request.session['role'] == 'supplier'):
            role_supplier = True
    except:
        return redirect('/')

    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select distinct username_supplier from riwayat_status_pesanan where no_pesanan='" + nomor + "'")
    supplier = namedtuplefetchall(cursor)[0]
    username_supplier = supplier.username_supplier

    cursor.execute("insert into riwayat_status_pesanan values ('PRO-SUP','" + nomor + "', '" + username_supplier +"', '" + tanggal + "')")
    cursor.execute("select distinct tsd.nomor, rsp.no_pesanan, p.nama, tsd.tanggal, tsd.total_berat, tsd.total_item, psd.total_harga, rsp.kode_status_pesanan, rsp.tanggal as tanggal_riwayat from transaksi_sumber_daya tsd, pesanan_sumber_daya psd, riwayat_status_pesanan rsp, pengguna p, admin_satgas adm where tsd.nomor=psd.nomor_pesanan and rsp.no_pesanan=psd.nomor_pesanan and psd.username_admin_satgas=adm.username and adm.username=p.username and (rsp.no_pesanan, rsp.tanggal) in (select rsp.no_pesanan, max(rsp.tanggal) as tanggal_riwayat from riwayat_status_pesanan rsp group by rsp.no_pesanan) and username_supplier= '" + username + "' group by tsd.nomor, rsp.no_pesanan, p.nama, tsd.tanggal, tsd.total_berat, tsd.total_item, psd.total_harga, rsp.kode_status_pesanan order by tsd.nomor")
    hasil=namedtuplefetchall(cursor)
    cursor.close()

    message = "Pesanan berhasil diproses"

    argument = {
        'table' : hasil,
        'role_supplier' : role_supplier,
        'message' : message,
    }
    return render(request, "listPesananSD.html", argument)

def selesaiMasalahPesananSD(request, nomor):
    nomor = str(nomor)
    tanggal = str(datetime.now().date())
    cursor = connection.cursor()
    role_supplier = False

    try:
        if (request.session['role'] == 'supplier'):
            role_supplier = True
    except:
        return redirect('/')

    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select distinct username_supplier from riwayat_status_pesanan where no_pesanan='" + nomor + "'")
    supplier = namedtuplefetchall(cursor)[0]
    username_supplier = supplier.username_supplier

    cursor.execute("insert into riwayat_status_pesanan values ('MAS-SUP','" + nomor + "', '" + username_supplier +"', '" + tanggal + "')")
    cursor.execute("select distinct tsd.nomor, rsp.no_pesanan, p.nama, tsd.tanggal, tsd.total_berat, tsd.total_item, psd.total_harga, rsp.kode_status_pesanan, rsp.tanggal as tanggal_riwayat from transaksi_sumber_daya tsd, pesanan_sumber_daya psd, riwayat_status_pesanan rsp, pengguna p, admin_satgas adm where tsd.nomor=psd.nomor_pesanan and rsp.no_pesanan=psd.nomor_pesanan and psd.username_admin_satgas=adm.username and adm.username=p.username and (rsp.no_pesanan, rsp.tanggal) in (select rsp.no_pesanan, max(rsp.tanggal) as tanggal_riwayat from riwayat_status_pesanan rsp group by rsp.no_pesanan) and username_supplier= '" + username + "' group by tsd.nomor, rsp.no_pesanan, p.nama, tsd.tanggal, tsd.total_berat, tsd.total_item, psd.total_harga, rsp.kode_status_pesanan order by tsd.nomor")
    hasil=namedtuplefetchall(cursor)
    cursor.close()

    message = "Pesanan berhasil diselesaikan (dengan masalah)"

    argument = {
        'table' : hasil,
        'role_supplier' : role_supplier,
        'message' : message,
    }
    return render(request, "listPesananSD.html", argument)

def selesaiPesananSD(request, nomor):
    nomor = str(nomor)
    tanggal = str(datetime.now().date())
    cursor = connection.cursor()
    role_supplier = False

    try:
        if (request.session['role'] == 'supplier'):
            role_supplier = True
    except:
        return redirect('/')

    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select distinct username_supplier from riwayat_status_pesanan where no_pesanan='" + nomor + "'")
    supplier = namedtuplefetchall(cursor)[0]
    username_supplier = supplier.username_supplier

    cursor.execute("insert into riwayat_status_pesanan values ('FIN-SUP','" + nomor + "', '" + username_supplier +"', '" + tanggal + "')")
    cursor.execute("select distinct tsd.nomor, rsp.no_pesanan, p.nama, tsd.tanggal, tsd.total_berat, tsd.total_item, psd.total_harga, rsp.kode_status_pesanan, rsp.tanggal as tanggal_riwayat from transaksi_sumber_daya tsd, pesanan_sumber_daya psd, riwayat_status_pesanan rsp, pengguna p, admin_satgas adm where tsd.nomor=psd.nomor_pesanan and rsp.no_pesanan=psd.nomor_pesanan and psd.username_admin_satgas=adm.username and adm.username=p.username and (rsp.no_pesanan, rsp.tanggal) in (select rsp.no_pesanan, max(rsp.tanggal) as tanggal_riwayat from riwayat_status_pesanan rsp group by rsp.no_pesanan) and username_supplier= '" + username + "' group by tsd.nomor, rsp.no_pesanan, p.nama, tsd.tanggal, tsd.total_berat, tsd.total_item, psd.total_harga, rsp.kode_status_pesanan order by tsd.nomor")
    hasil=namedtuplefetchall(cursor)
    cursor.close()

    message = "Pesanan berhasil diselesaikan"

    argument = {
        'table' : hasil,
        'role_supplier' : role_supplier,
        'message' : message,
    }
    return render(request, "listPesananSD.html", argument)