from django.urls import path
from .views import list_stok_faskes, delete_stok_faskes, create_stok_faskes, update_stok_faskes, get_nama_faskes, get_nama_item

urlpatterns = [
    path('stokFaskes/', list_stok_faskes, name="list_stok_faskes"),
    path('deleteStokFaskes/<str:kode_faskes>/<str:kode_item_sumber_daya>', delete_stok_faskes, name="delete_stok_faskes"),
    path('tambahStokFaskes/', create_stok_faskes, name="create_stok_faskes"),
    path('updateStokFaskes/<str:kode_faskes>/<str:kode_item_sumber_daya>/', update_stok_faskes, name="update_stok_faskes"),
    path('getFaskes/<str:kode_faskes>', get_nama_faskes, name="get_nama_faskes"),
    path('getItem/<str:kode_item>', get_nama_item, name="get_nama_item"),
]