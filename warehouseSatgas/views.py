from django.shortcuts import render, redirect
from django.db import connection
from django.http import JsonResponse
from collections import namedtuple
# from .forms import *

# Create your views here.

# general function
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()] 

# read list warehouse satgas
def listWarehouseSatgas(request):
    cursor = connection.cursor()
    role_admin_satgas = False
    role_supplier = False
    role_petugas_distribusi = False
    role_petugas_faskes = False

    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
        if (request.session['role'] == 'supplier'):
            role_supplier = True
        if (request.session['role'] == 'petugas_distribusi'):
            role_petugas_distribusi = True
        if (request.session['role'] == 'petugas_faskes'):
            role_petugas_faskes = True
    except:
        return redirect('/')

    username = str(request.session['username'])
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select row_number() over(order by id_lokasi asc) as no, id_lokasi, provinsi, concat(jalan_no, ', ', kelurahan, ', ', kecamatan, ', ', kabkot) as lokasi from warehouse_provinsi join lokasi on id = id_lokasi")
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    argument = {
        'table' : hasil,
        'role_admin_satgas' : role_admin_satgas,
        'role_supplier' : role_supplier,
        'role_petugas_distribusi' : role_petugas_distribusi,
        'role_petugas_faskes' : role_petugas_faskes,
    }
    return render(request, "listWarehouseSatgas.html", argument)


# create new warehouse satgas
def createWarehouseSatgas(request):
    message =""
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select distinct provinsi from lokasi where id not in (select id_lokasi from warehouse_provinsi) order by provinsi asc")
    provinsi_view = namedtuplefetchall(cursor)
    cursor.execute("select * from lokasi order by id asc")
    lokasi_view = namedtuplefetchall(cursor)
    
    if request.method =='POST':
        id_lokasi = request.POST['id_lokasi']
        try :
            cursor.execute("insert into warehouse_provinsi values ('"+id_lokasi+"')")
            cursor.close()
            return redirect('/WarehouseSatgas')
        except Exception as e:
            message = 'Terdapat data yang belum diisi, silahkan lengkapi data terlebih dahulu'

    args = {
        'provinsi': provinsi_view,
        'lokasi': lokasi_view,
        'message' : message
    }
    return render(request, "createWarehouseSatgas.html", args)

# delete warehouse satgas
def deleteWarehouseSatgas(request, id_lokasi):
    id_lokasi = str(id_lokasi)
    cursor = connection.cursor()
    role_admin_satgas = False

    try:
        if (request.session['role'] == 'admin_satgas'):
            role_admin_satgas = True
    except:
        return redirect('/')

    cursor.execute("set search_path to wikisidia")
    cursor.execute("delete from warehouse_provinsi where id_lokasi = '"+id_lokasi+"'")
    cursor.execute("select row_number() over(order by id_lokasi asc) as no, id_lokasi, provinsi, concat(jalan_no, ', ', kelurahan, ', ', kecamatan, ', ', kabkot) as lokasi from warehouse_provinsi join lokasi on id = id_lokasi")
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    message = "Data berhasil dihapus"
    argument = {
        'table' : hasil,
        'role_admin_satgas' : role_admin_satgas,
        'message' : message
    }
    return render(request, "listWarehouseSatgas.html", argument)

# update warehouse satgas
def updateWarehouseSatgas(request, provinsi, id):
    message =""
    provinsi = str(provinsi)
    id = str(id)
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select id from lokasi where provinsi = '" + provinsi + "'")
    id_lokasi_view = namedtuplefetchall(cursor)
    cursor.execute("select * from lokasi order by id asc")
    lokasi_view = namedtuplefetchall(cursor)
    
    if request.method =='POST':
        id_lokasi_baru = request.POST['id_lokasi']
        try :
            cursor.execute("update warehouse_provinsi set id_lokasi = '"+ id_lokasi_baru +"' where id_lokasi = '" + id + "'")
            cursor.close()
            return redirect('/WarehouseSatgas')
        except Exception as e:
            message = "Terdapat kesalahan data Warehouse Satgas yang diupdate"

    args = {
        'provinsi': provinsi,
        'id': id,
        'id_lokasi': id_lokasi_view,
        'lokasi': lokasi_view,
        'message' : message
    }
    return render(request, "updateWarehouseSatgas.html", args)

# generate id dari provinsi
def getIdLokasi(request, provinsi):
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select id from lokasi where provinsi = '" + provinsi + "'")
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    argument = {
        'table' : hasil,
    }

    return JsonResponse(hasil, safe=False)

# generate otomatis id dan lokasi
def getLokasiSatgas(request, id_lokasi):
    cursor = connection.cursor()
    cursor.execute("set search_path to wikisidia")
    cursor.execute("select id, concat(jalan_no, ', ', kelurahan, ', ', kecamatan, ', ', kabkot) as lokasi from lokasi where id = '" + id_lokasi + "'")
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    argument = {
        'table_lokasi' : hasil,
    }

    return JsonResponse(hasil, safe=False)