--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.26
-- Dumped by pg_dump version 11.9 (Debian 11.9-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: sidia; Type: SCHEMA; Schema: -; Owner: db202b17
--

CREATE SCHEMA sidia;


ALTER SCHEMA sidia OWNER TO db202b17;

set search_path to sidia;

CREATE OR REPLACE FUNCTION sidia.cek_jumlah_item_pesanan_sumber_daya()
RETURNS trigger AS
    $$
    DECLARE
        temp_row RECORD;
        temp_berat_satuan INTEGER;
    	temp_harga_satuan INTEGER;
    	berat_total INTEGER;
    	jumlah_total INTEGER;
    BEGIN
        FOR temp_row IN
        			SELECT no_urut, jumlah_item, kode_item_sumber_daya, no_transaksi_sumber_daya
        			FROM DAFTAR_ITEM
    	                  	WHERE no_transaksi_sumber_daya = NEW.no_transaksi_sumber_daya
    	             LOOP
    	    	    	SELECT harga_satuan, berat_satuan 
    	    	    	     INTO temp_harga_satuan, temp_berat_satuan 
    	    	    	FROM ITEM_SUMBER_DAYA
    	    	    	WHERE kode = temp_row.kode_item_sumber_daya;

    	    	    	UPDATE DAFTAR_ITEM di
    	    	    	SET berat_kumulatif = temp_berat_satuan * temp_row.jumlah_item,
     	    	    	     harga_kumulatif = temp_harga_satuan * temp_row.jumlah_item
    	    	    	WHERE di.no_transaksi_sumber_daya = temp_row.no_transaksi_sumber_daya AND di.no_urut = temp_row.no_urut;
    	            END LOOP;

                    SELECT SUM(berat_kumulatif), SUM(jumlah_item) INTO berat_total, jumlah_total 
    	              FROM DAFTAR_ITEM
    	              WHERE no_transaksi_sumber_daya =  NEW.no_transaksi_sumber_daya;
                            -- update total berat sm total itemnya
    	              UPDATE TRANSAKSI_SUMBER_DAYA
                        SET total_berat = berat_total, total_item = jumlah_total 
                        WHERE nomor = NEW.no_transaksi_sumber_daya;

                        RETURN NEW;
    END;

    $$
LANGUAGE plpgsql;

ALTER FUNCTION sidia.cek_jumlah_item_pesanan_sumber_daya() OWNER TO db202b17;
--
-- Name: jumlah_batch_check(); Type: FUNCTION; Schema: sidia; Owner: db202b17
--
CREATE OR REPLACE FUNCTION sidia.cek_jumlah_item_pesanan_sumber_daya()
RETURNS trigger AS
    $$
    DECLARE
        temp_row RECORD;
        temp_berat_satuan INTEGER;
    	temp_harga_satuan INTEGER;
    	berat_total INTEGER;
    	jumlah_total INTEGER;
    BEGIN
        FOR temp_row IN
        			SELECT no_urut, jumlah_item, kode_item_sumber_daya, no_transaksi_sumber_daya
        			FROM DAFTAR_ITEM
    	                  	WHERE no_transaksi_sumber_daya = NEW.no_transaksi_sumber_daya
    	             LOOP
    	    	    	SELECT harga_satuan, berat_satuan 
    	    	    	     INTO temp_harga_satuan, temp_berat_satuan 
    	    	    	FROM ITEM_SUMBER_DAYA
    	    	    	WHERE kode = temp_row.kode_item_sumber_daya;

    	    	    	UPDATE DAFTAR_ITEM di
    	    	    	SET berat_kumulatif = temp_berat_satuan * temp_row.jumlah_item,
     	    	    	     harga_kumulatif = temp_harga_satuan * temp_row.jumlah_item
    	    	    	WHERE di.no_transaksi_sumber_daya = temp_row.no_transaksi_sumber_daya AND di.no_urut = temp_row.no_urut;
    	            END LOOP;

                    SELECT SUM(berat_kumulatif), SUM(jumlah_item) INTO berat_total, jumlah_total 
    	              FROM DAFTAR_ITEM
    	              WHERE no_transaksi_sumber_daya =  NEW.no_transaksi_sumber_daya;
                            -- update total berat sm total itemnya
    	              UPDATE TRANSAKSI_SUMBER_DAYA
                        SET total_berat = berat_total, total_item = jumlah_total 
                        WHERE nomor = NEW.no_transaksi_sumber_daya;

                        RETURN NEW;
    END;

    $$
LANGUAGE plpgsql;
CREATE OR REPLACE FUNCTION sidia.cek_jumlah_batch()
RETURNS trigger AS
    $$
    DECLARE
        temp_row RECORD;
        temp_berat INTEGER;
        maks_kendaraan_new  INTEGER;
        berat_total_batch INTEGER;
    BEGIN
        berat_total_batch = 0;
        FOR temp_row IN
        	SELECT no_kendaraan
        	FROM BATCH_PENGIRIMAN
    	WHERE no_transaksi_sumber_daya = NEW.no_transaksi_sumber_daya
        LOOP
    	SELECT berat_maksimum INTO temp_berat
    	FROM KENDARAAN
    	WHERE nomor = temp_row.no_kendaraan;

             berat_total_batch = berat_total_batch  + temp_berat;

             END LOOP;

        SELECT total_berat INTO temp_berat
        FROM TRANSAKSI_SUMBER_DAYA
        WHERE nomor = NEW.no_transaksi_sumber_daya;

        SELECT berat_maksimum INTO maks_kendaraan_new 
        FROM KENDARAN
        WHERE nomor = NEW.no_kendaraan;

        IF(berat_total_batch >= temp_berat) 
            THEN RAISE EXCEPTION 'Batch sudah dapat menampung total berat untuk transaksi ini';
        ELSE 
            berat_total_batch = berat_total_batch + maks_kendaraan_new;
            IF(berat_total_batch < temp_berat) 
                THEN RAISE NOTICE 'Batch masih belum cukup menampung transaksi ini';
            ELSE RAISE NOTICE 'Batch sudah dapat menampung total berat untuk transaksi ini';
            END IF;
        END IF;

        RETURN NEW;
    END;

    $$
LANGUAGE plpgsql;

ALTER FUNCTION sidia.cek_jumlah_batch() OWNER TO db202b17;

--
-- Name: lokasi_batch_check(); Type: FUNCTION; Schema: sidia; Owner: db202b17
--
CREATE OR REPLACE FUNCTION sidia.cek_jumlah_batch()
RETURNS trigger AS
    $$
    DECLARE
        id_asal VARCHAR;
        id_tujuan VARCHAR;
        provinsi_tujuan VARCHAR;

    BEGIN
-- id_tujuan : lokasi tujuan batch pengiriman adalah lokasi FASKES yang mengajukan permohonan sumber daya.
        SELECT id_lokasi, provinsi 
            INTO id_tujuan, provinsi_tujuan
        FROM BATCH_PENGIRIMAN bp
        JOIN TRANSAKSI_SUMBER_DAYA tsd on bp.nomor_transaksi_sumber_daya=tsd.nomor
        JOIN PERMOHONAN_SUMBER_DAYA_FASKES psdf on tsd.nomor = psdf.no_transaksi_sumber_daya
        JOIN PETUGAS_FASKES pf on psdf.username_petugas_faskes = pf.username
        JOIN FASKES f on pf.username=f.username_petugas 
        JOIN LOKASI l on f.id_lokasi=l.id
        WHERE bp.nomor_transaksi_sumber_daya = NEW.nomor_transaksi_sumber_daya;
        
 -- id_asal : lokasi asal batch pengiriman adalah warehouse satgas yang terletak di provinsi yang sama dengan faskes yang mengajukan permohonan sumber daya
        SELECT id_lokasi INTO id_asal
        FROM warehouse_provinsi wp
        JOIN lokasi l ON wp.id_lokasi=l.id
        WHERE l.provinsi = provinsi_tujuan;

 -- update id_lokasi_asal dan id_lokasi_tujian.  Jika nomor transaksi sumber daya belum terdaftar di  permohonan sumber daya faskes, maka id_lokasi_asal dan id_lokasi_tujian akan kosong.
        UPDATE BATCH_PENGIRIMAN 
        SET id_lokasi_asal = id_asal, id_lokasi_tujuan=id_tujuan
        WHERE kode = NEW.kode ;

        RETURN NEW;
        
    END;

    $$
LANGUAGE plpgsql;




ALTER FUNCTION sidia.cek_jumlah_batch() OWNER TO db202b17;

--
-- Name: cek_stock_sumber_daya(); Type: FUNCTION; Schema: sidia; Owner: db202b17
--

CREATE OR REPLACE FUNCTION sidia.cek_stock_sumber_daya()
RETURNS TRIGGER AS
$$
              DECLARE
-- A. Variable buat nge update berat dan harga kumulatif setiap DAFTAR_ITEM
    	              temp_berat_satuan INTEGER;
    	              temp_harga_satuan INTEGER;
-- B. Variable  buat nge update total berat dan item di TRANSAKSI
    	              berat_total INTEGER;
    	              jumlah_total INTEGER;
-- C. Variable buat ngecek stock di warehouse provinsi
    	              temp_row RECORD;
    	              is_valid BOOLEAN;
    	              id_lokasi_provinsi VARCHAR(5);
    	    	temp_stock_item INTEGER;
	BEGIN
-- A. Kode di bawah buat nge update berat dan harga kumulatif setiap DAFTAR_ITEM
                        -- ngambil tiap daftar item di transaksi yg sama
        	             FOR temp_row IN
        			SELECT no_urut, jumlah_item, kode_item_sumber_daya, no_transaksi_sumber_daya
        			FROM DAFTAR_ITEM
    	                  	WHERE no_transaksi_sumber_daya = NEW.nomor_permohonan 
    	             LOOP
    	    	    	SELECT harga_satuan, berat_satuan 
    	    	    	     INTO temp_harga_satuan, temp_berat_satuan 
    	    	    	FROM ITEM_SUMBER_DAYA
    	    	    	WHERE kode = temp_row.kode_item_sumber_daya;

    	    	    	UPDATE DAFTAR_ITEM di
    	    	    	SET berat_kumulatif = temp_berat_satuan * temp_row.jumlah_item,
     	    	    	     harga_kumulatif = temp_harga_satuan * temp_row.jumlah_item
    	    	    	WHERE di.no_transaksi_sumber_daya = temp_row.no_transaksi_sumber_daya AND di.no_urut = temp_row.no_urut;
    	            END LOOP;

-- B. Kode di bawah buat nge update total berat dan item di TRANSAKSI  
                        -- ambil semua daftar item yg transaksinya sama kek yg dibuat           
    	              SELECT SUM(berat_kumulatif), SUM(jumlah_item) INTO berat_total, jumlah_total 
    	              FROM DAFTAR_ITEM
    	              WHERE no_transaksi_sumber_daya =  NEW.nomor_permohonan;
                            -- update total berat sm total itemnya
    	              UPDATE TRANSAKSI_SUMBER_DAYA
                        SET total_berat = berat_total, total_item = jumlah_total 
                        WHERE nomor = NEW.nomor_permohonan;

-- C. Kode di bawah buat ngecek stock di warehouse provinsi
    	    	is_valid = TRUE;
                    -- ambil lokasi si petugas fakses yg ngajuin
    	              SELECT id_lokasi INTO id_lokasi_provinsi
     	              FROM FASKES
    	              WHERE username_petugas IN
    	              	(SELECT username_petugas_faskes
    	              	FROM PERMOHONAN_SUMBER_DAYA_FASKES
    	              	WHERE no_transaksi_sumber_daya =  NEW.nomor_permohonan);

		FOR temp_row IN
        			SELECT jumlah_item, kode_item_sumber_daya
        			FROM DAFTAR_ITEM
    	                  	WHERE no_transaksi_sumber_daya = NEW.nomor_permohonan 
    	             LOOP
    	    	    	SELECT jumlah INTO temp_stock_item 
    	    	    	FROM STOCK_WAREHOUSE_PROVINSI swp
    	    	    	WHERE swp.id_lokasi_warehouse = id_lokasi_provinsi 
    	    	    	AND swp.kode_item_sumber_daya = temp_row.kode_item_sumber_daya;
    	    	    	IF(temp_stock_item < temp_row.jumlah_item) THEN 
                            is_valid = FALSE;
    	    	    	END IF;
    	            END LOOP;
    	    	IF(is_valid) THEN NEW.kode_status_permohonan = 'REQ';
    	    	ELSE NEW.kode_status_permohonan = 'WAI';
                END IF;
    	            RETURN NEW;
              END;
$$
LANGUAGE plpgsql;


ALTER FUNCTION sidia.cek_stock_sumber_daya() OWNER TO db202b17;

--
-- Name: username_check(); Type: FUNCTION; Schema: sidia; Owner: db202b17
--


CREATE OR REPLACE FUNCTION sidia.cek_username()
RETURNS TRIGGER AS
$$
              DECLARE
    	              count_user INTEGER;
	BEGIN
    	-- TG_OP: variabel untuk mengetahui apakah
    	-- jenis operasinya itu INSERT, UPDATE,
    	-- atau yang lainnya.
    	-- NEW adalah row yang sedang di-input/update.
    	    	IF (TG_OP = 'INSERT') THEN
                                	SELECT COUNT(*) INTO count_user
                                	FROM PENGGUNA
                                	WHERE  username = NEW.username;
        	                           
        	                            IF (count_user > 0) THEN
            	                                          RAISE EXCEPTION 'Username Exist';
        	                            END IF;
        	                            RETURN NEW;
    	              END IF;
              END;
$$
LANGUAGE plpgsql;




ALTER FUNCTION sidia.cek_username() OWNER TO db202b17;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin_satgas; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.admin_satgas (
    username character varying(50) NOT NULL
);


ALTER TABLE sidia.admin_satgas OWNER TO db202b17;

--
-- Name: batch_pengiriman; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.batch_pengiriman (
    kode character varying(5) NOT NULL,
    username_satgas character varying(50) NOT NULL,
    username_petugas_distribusi character varying(50) NOT NULL,
    tanda_terima character varying(50) NOT NULL,
    nomor_transaksi_sumber_daya integer NOT NULL,
    id_lokasi_asal character varying(5) NOT NULL,
    id_lokasi_tujuan character varying(5) NOT NULL,
    no_kendaraan character varying(10) NOT NULL
);


ALTER TABLE sidia.batch_pengiriman OWNER TO db202b17;

--
-- Name: daftar_item; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.daftar_item (
    no_transaksi_sumber_daya integer NOT NULL,
    no_urut integer NOT NULL,
    jumlah_item integer NOT NULL,
    kode_item_sumber_daya character varying(5),
    berat_kumulatif integer,
    harga_kumulatif integer
);


ALTER TABLE sidia.daftar_item OWNER TO db202b17;

--
-- Name: faskes; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.faskes (
    kode_faskes_nasional character varying(15) NOT NULL,
    id_lokasi character varying(5),
    username_petugas character varying(50),
    kode_tipe_faskes character varying(3)
);


ALTER TABLE sidia.faskes OWNER TO db202b17;

--
-- Name: item_sumber_daya; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.item_sumber_daya (
    kode character varying(5) NOT NULL,
    username_supplier character varying(50) NOT NULL,
    nama character varying(50) NOT NULL,
    harga_satuan integer NOT NULL,
    berat_satuan integer NOT NULL,
    kode_tipe_item character varying(3) NOT NULL
);


ALTER TABLE sidia.item_sumber_daya OWNER TO db202b17;

--
-- Name: kendaraan; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.kendaraan (
    nomor character varying(10) NOT NULL,
    nama character varying(30) NOT NULL,
    jenis_kendaraan character varying(15) NOT NULL,
    berat_maksimum integer NOT NULL
);


ALTER TABLE sidia.kendaraan OWNER TO db202b17;

--
-- Name: lokasi; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.lokasi (
    id character varying(5) NOT NULL,
    provinsi character varying(20) NOT NULL,
    kabkot character varying(20) NOT NULL,
    kecamatan character varying(20) NOT NULL,
    kelurahan character varying(20) NOT NULL,
    jalan_no character varying(20) NOT NULL
);


ALTER TABLE sidia.lokasi OWNER TO db202b17;

--
-- Name: manifes_item; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.manifes_item (
    kode_batch_pengiriman character varying(5) NOT NULL,
    no_urut integer NOT NULL,
    kode_item_sumber_daya character varying(5),
    jumlah_item integer NOT NULL,
    berat_kumulatif integer
);


ALTER TABLE sidia.manifes_item OWNER TO db202b17;

--
-- Name: pengguna; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.pengguna (
    username character varying(50) NOT NULL,
    password character varying(20) NOT NULL,
    nama character varying(50) NOT NULL,
    alamat_kel character varying(20) NOT NULL,
    alamat_kec character varying(20) NOT NULL,
    alamat_kabkot character varying(20) NOT NULL,
    alamat_prov character varying(20) NOT NULL,
    no_telepon character varying(15) NOT NULL
);


ALTER TABLE sidia.pengguna OWNER TO db202b17;

--
-- Name: permohonan_sumber_daya_faskes; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.permohonan_sumber_daya_faskes (
    no_transaksi_sumber_daya integer NOT NULL,
    username_petugas_faskes character varying(50) NOT NULL,
    catatan text
);


ALTER TABLE sidia.permohonan_sumber_daya_faskes OWNER TO db202b17;

--
-- Name: pesanan_sumber_daya; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.pesanan_sumber_daya (
    nomor_pesanan integer NOT NULL,
    username_admin_satgas character varying(50),
    total_harga integer
);


ALTER TABLE sidia.pesanan_sumber_daya OWNER TO db202b17;

--
-- Name: petugas_distribusi; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.petugas_distribusi (
    username character varying(50) NOT NULL,
    no_sim character varying(15) NOT NULL
);


ALTER TABLE sidia.petugas_distribusi OWNER TO db202b17;

--
-- Name: petugas_faskes; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.petugas_faskes (
    username character varying(50) NOT NULL
);


ALTER TABLE sidia.petugas_faskes OWNER TO db202b17;

--
-- Name: riwayat_status_pengiriman; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.riwayat_status_pengiriman (
    kode_status_batch_pengiriman character varying(3) NOT NULL,
    kode_batch character varying(5) NOT NULL,
    tanggal timestamp without time zone NOT NULL
);


ALTER TABLE sidia.riwayat_status_pengiriman OWNER TO db202b17;

--
-- Name: riwayat_status_permohonan; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.riwayat_status_permohonan (
    kode_status_permohonan character varying(3) NOT NULL,
    nomor_permohonan integer NOT NULL,
    username_admin character varying(50),
    tanggal date NOT NULL
);


ALTER TABLE sidia.riwayat_status_permohonan OWNER TO db202b17;

--
-- Name: riwayat_status_pesanan; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.riwayat_status_pesanan (
    kode_status_pesanan character varying(7) NOT NULL,
    no_pesanan integer NOT NULL,
    username_supplier character varying(50) NOT NULL,
    tanggal date NOT NULL
);


ALTER TABLE sidia.riwayat_status_pesanan OWNER TO db202b17;

--
-- Name: status_batch_pengiriman; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.status_batch_pengiriman (
    kode character varying(3) NOT NULL,
    nama character varying(25) NOT NULL
);


ALTER TABLE sidia.status_batch_pengiriman OWNER TO db202b17;

--
-- Name: status_permohonan; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.status_permohonan (
    kode character varying(3) NOT NULL,
    nama character varying(25) NOT NULL
);


ALTER TABLE sidia.status_permohonan OWNER TO db202b17;

--
-- Name: status_pesanan; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.status_pesanan (
    kode character varying(7) NOT NULL,
    nama character varying(25) NOT NULL
);


ALTER TABLE sidia.status_pesanan OWNER TO db202b17;

--
-- Name: stok_faskes; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.stok_faskes (
    kode_faskes character varying(15) NOT NULL,
    kode_item_sumber_daya character varying(5) NOT NULL,
    jumlah integer NOT NULL
);


ALTER TABLE sidia.stok_faskes OWNER TO db202b17;

--
-- Name: stok_warehouse_provinsi; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.stok_warehouse_provinsi (
    id_lokasi_warehouse character varying(5) NOT NULL,
    jumlah integer NOT NULL,
    kode_item_sumber_daya character varying(5) NOT NULL
);


ALTER TABLE sidia.stok_warehouse_provinsi OWNER TO db202b17;

--
-- Name: supplier; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.supplier (
    username character varying(50) NOT NULL,
    nama_organisasi character varying(20) NOT NULL
);


ALTER TABLE sidia.supplier OWNER TO db202b17;

--
-- Name: tipe_faskes; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.tipe_faskes (
    kode character varying(3) NOT NULL,
    nama_tipe character varying(30) NOT NULL
);


ALTER TABLE sidia.tipe_faskes OWNER TO db202b17;

--
-- Name: tipe_item; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.tipe_item (
    kode character varying(3) NOT NULL,
    nama character varying(50) NOT NULL
);


ALTER TABLE sidia.tipe_item OWNER TO db202b17;

--
-- Name: transaksi_sumber_daya; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.transaksi_sumber_daya (
    nomor integer NOT NULL,
    tanggal date NOT NULL,
    total_berat integer,
    total_item integer
);


ALTER TABLE sidia.transaksi_sumber_daya OWNER TO db202b17;

--
-- Name: tsd_id_seq; Type: SEQUENCE; Schema: sidia; Owner: db202b17
--

CREATE SEQUENCE sidia.tsd_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sidia.tsd_id_seq OWNER TO db202b17;

--
-- Name: tsd_id_seq; Type: SEQUENCE OWNED BY; Schema: sidia; Owner: db202b17
--

ALTER SEQUENCE sidia.tsd_id_seq OWNED BY sidia.transaksi_sumber_daya.nomor;


--
-- Name: warehouse_provinsi; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.warehouse_provinsi (
    id_lokasi character varying(5) NOT NULL
);


ALTER TABLE sidia.warehouse_provinsi OWNER TO db202b17;

--
-- Name: warehouse_supplier; Type: TABLE; Schema: sidia; Owner: db202b17
--

CREATE TABLE sidia.warehouse_supplier (
    id_lokasi character varying(5) NOT NULL,
    username_supplier character varying(50) NOT NULL
);


ALTER TABLE sidia.warehouse_supplier OWNER TO db202b17;

--
-- Name: transaksi_sumber_daya nomor; Type: DEFAULT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.transaksi_sumber_daya ALTER COLUMN nomor SET DEFAULT nextval('sidia.tsd_id_seq'::regclass);


--
-- Data for Name: admin_satgas; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.admin_satgas (username) FROM stdin;
mteape5
vturbarda
walfonsettie
tweekesj
sdemangem
smitcheson8
ddivellc
\.


--
-- Data for Name: batch_pengiriman; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.batch_pengiriman (kode, username_satgas, username_petugas_distribusi, tanda_terima, nomor_transaksi_sumber_daya, id_lokasi_asal, id_lokasi_tujuan, no_kendaraan) FROM stdin;
BP001	mteape5	avasler0	sudah diterima oleh satgas xxxx	1	L0001	L0006	K01
BP002	vturbarda	jlorans1	sudah diterima oleh satgas xxxx	2	L0002	L0007	K02
BP003	walfonsettie	wrobbeke2	sudah diterima oleh satgas xxxx	3	L0003	L0008	K03
BP004	tweekesj	lmcalindon3	sudah diterima oleh satgas xxxx	4	L0004	L0009	K04
BP005	sdemangem	awiddop4	sudah diterima oleh satgas xxxx	5	L0005	L0010	K05
BP006	mteape5	avasler0	sudah diterima oleh satgas xxxx	6	L0001	L0006	K01
BP007	vturbarda	jlorans1	sudah diterima oleh satgas xxxx	7	L0002	L0007	K02
BP008	mteape5	avasler0	sudah diterima oleh satgas xxxx	2	L0001	L0006	K01
BP009	mteape5	avasler0	sudah diterima oleh satgas xxxx	2	L0001	L0006	K01
\.


--
-- Data for Name: daftar_item; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.daftar_item (no_transaksi_sumber_daya, no_urut, jumlah_item, kode_item_sumber_daya, berat_kumulatif, harga_kumulatif) FROM stdin;
1	1	147	IT001	5000	5870000
2	2	170	IT002	2500	4320000
3	3	130	IT003	150	12330000
4	4	144	IT005	150	42100000
5	5	169	IT006	601	12200000
6	6	50	IT007	100	5870000
7	7	100	IT010	200	4320000
8	8	158	IT011	180	12330000
9	9	130	IT014	300	42100000
10	10	50	IT015	50	12200000
13	11	25	IT004	25	19250000
\.


--
-- Data for Name: faskes; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.faskes (kode_faskes_nasional, id_lokasi, username_petugas, kode_tipe_faskes) FROM stdin;
FAS1	L0001	tswynfen6	RUJ
FAS2	L0002	msmietond	IDD
FAS3	L0003	gpierseg	KLI
FAS4	L0004	tcauseyh	PUS
FAS5	L0005	givashkovk	RUJ
\.


--
-- Data for Name: item_sumber_daya; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.item_sumber_daya (kode, username_supplier, nama, harga_satuan, berat_satuan, kode_tipe_item) FROM stdin;
IT001	kmebes9	Baju APD	300000	1	APD
IT002	gritchleyb	Ventilator	20000000	10	VEN
IT003	lharringn	Bed abs 1 crank	8650000	11	BED
IT004	skitleyi	Masker N99	770000	1	MSK
IT005	mopenshaw7	Masker N95	770000	1	MSK
IT006	kmebes9	Masker Kain Tiga Lapis	880000	1	MSK
IT007	gritchleyb	AstraZeneca	3500000	1	VAK
IT008	lharringn	Sinovac	10000000	1	VAK
IT009	skitleyi	Pfizer	13750000	1	VAK
IT010	mopenshaw7	Vaksin Gotong Royong	16000000	1	VAK
IT011	kmebes9	Alat tes PCR	162000	1	PCR
IT012	gritchleyb	Alat rapid test	52000	1	RAP
IT013	lharringn	Face Shield	100000	1	APD
IT014	skitleyi	Goggle	450000	1	APD
IT015	mopenshaw7	Bed abs 3 crank	6700000	11	BED
\.


--
-- Data for Name: kendaraan; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.kendaraan (nomor, nama, jenis_kendaraan, berat_maksimum) FROM stdin;
K01	Grand max	Mini Bus	700
K02	APV	Mini Bus	550
K03	Carry	Pick Up	800
K04	Axor	Tronton	10000
K05	Cmoto	Motor Kargo	200
\.


--
-- Data for Name: lokasi; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.lokasi (id, provinsi, kabkot, kecamatan, kelurahan, jalan_no) FROM stdin;
L0001	DKI Jakarta	Jakarta Timur	Kramat Jati	Balekambang	Jl. Condet, No. 30
L0002	Jawa Barat	Bandung	Cibiru	Cipadung	Jl. Sukajaya, No. 2A
L0003	Jawa Tengah	Semarang	Tugu	Karanganyar	Jl. Kartini, No. 13
L0004	Jawa Timur	Surabaya	Sukolilo	Medokan	Gg. Mangga, No. 44
L0005	Banten	Tangerang	Pinang	Pakojan	Jl. Kenanga, No. 33
L0006	Sumatra Utara	Medan	Medan Perjuangan	Tegal Rejo	Jl. Kiwi, No. 12
L0007	Sumatra Barat	Padang	Padang Utara	Air Tawar Barat	Jl. Sawahan, No. 1
L0008	DI Yogyakarta	Yogyakarta	Kotagede	Purbayan	Jl. Pepaya, No. 21
L0009	Bali	Denpasar	Denpasar Selatan	Sanur Kaja	Jl. Puri, No. 343
L0010	Sulawesi Selatan	Makassar	Manggala	Antang	Jl. Dewi, No. 89
\.


--
-- Data for Name: manifes_item; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.manifes_item (kode_batch_pengiriman, no_urut, kode_item_sumber_daya, jumlah_item, berat_kumulatif) FROM stdin;
BP001	1	IT001	147	5000
BP002	2	IT002	170	2500
BP005	3	IT003	130	150
BP002	4	IT005	144	150
BP001	5	IT006	169	601
BP001	6	IT007	50	100
BP001	7	IT010	100	200
BP004	8	IT011	158	180
BP003	9	IT014	130	300
BP004	10	IT015	50	50
\.


--
-- Data for Name: pengguna; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.pengguna (username, password, nama, alamat_kel, alamat_kec, alamat_kabkot, alamat_prov, no_telepon) FROM stdin;
avasler0	sh0nwIN	Budi rahman	Lhong Raya	Banda Raya	Banda Aceh	Aceh	087024199728
jlorans1	msJHq6LSQ	Budi supardi	Teladan Barat	Medan Kota	Medan	Sumatra Utara	082374789557
wrobbeke2	Rmsrmjwgl	Ahmad joko	Kedungpedaringan	Kepanjen	Malang	Jawa Timur	083253547011
lmcalindon3	dbPqvhxWD21S	Hendra ahmad	Pakansari	Cibinong	Bogor	Jawa Barat	081001632068
awiddop4	tA8hx1	Anto ahmad syarif	Manahan	Banjarsari	Surakarta	Jawa Tengah	083271263020
mteape5	6D1MLIP	Merle	Bojong Pondok Terong	Cipayung	Depok	Jawa Barat	083461002049
tswynfen6	lAwftdKs	Terrill	Geragahan	Lubuk Basung	Agam	Sumatra Barat	089129141187
mopenshaw7	gYWfwU	Melicent	Ogan Baru	Kertapati	Palembang	Sumatra Selatan	084016686047
smitcheson8	1Oip1e	Shirlee	Hadimulyo Barat	Metro Pusat	Metro	Lampung	083527096746
kmebes9	VwEl23qiHDr	Kara-lynn	Pancoran	Pancoran	Jakarta Selatan	DKI Jakarta	087563828525
vturbarda	wSrRlI	Vassily	Seberang Mesjid	Banjarmasin Tengah	Banjarmasin	Kalimantan Selatan	085226485952
gritchleyb	r08Gt9iB7	Guss	Penajam	Penajam	Penajam Paser Utara	Kalimantan Timur	089327002999
ddivellc	iakU5grW	Daryl	Ciroyom	Andir	Kota Bandung	Jawa Barat	089658180859
msmietond	UPpIOp53TBB	Maudie	Margasari	Karawaci	Kota Tangerang	Banten	089219312882
walfonsettie	tnyqzkTTEWqT	Willow	Sungaijawi	Pontianak Kota	Pontianak	Kalimantan Barat	086779515808
mcampanellif	JksRxFd1XU6j	Melosa	Langkai	Pahandut	Palangka Raya	Kalimantan Tengah	088759590834
gpierseg	6zsdP4M9ZQw	Gilda	Gianyar	Gianyar	Gianyar	Bali	082587318579
tcauseyh	qLQSRDJ	Tawnya	Pancor	Selong	Lombok Timur	Nusa Tenggara Barat	087095181079
skitleyi	vetzrUbyy	Sid	Alak	Alak	Kupang	Nusa Tenggara Timur	083725972169
tweekesj	kJg9yX	Terza	Pegangsaan	Menteng	Jakarta Pusat	DKI Jakarta	086197899477
givashkovk	bIQIeMN8Fc	Giffie	Maradekaya	Makassar	Makassar	Sulawesi Selatan	086722726434
ldrysdelll	hz7fpM	Lilyan	Molas	Bunaken	Manado	Sulawesi Utara	081207044082
sdemangem	BOjHJ3	Simeon	Mangga Dua	Nusaniwe	Ambon	Maluku	086354732619
lharringn	pRXgUl99	Lorianne	Duri Kepa	Kebon Jeruk	Jakarta Barat	DKI Jakarta	088887280441
laldredo	QvhaLN9dr5a	Lissy	Mandala	Jayapura Utara	Jayapura	Papua	087873951601
fdyltfhh	pRTgUl99	fadiya	Tanahbaru	Cibinong	Bogor	Jawa Barat	087973951601
xxarringn	pRTgUl97	latifah	Jatimulya	Tambun	Bekasi	Jawa Barat	088973151601
\.


--
-- Data for Name: permohonan_sumber_daya_faskes; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.permohonan_sumber_daya_faskes (no_transaksi_sumber_daya, username_petugas_faskes, catatan) FROM stdin;
1	tswynfen6	Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.
2	msmietond	Pellentesque eget nunc.
3	gpierseg	Suspendisse potenti.ajuiauia 
4	tcauseyh	Suspendisse potenti.
5	givashkovk	Fusce consequat. Nulla nisl. Nunc nisl.
6	tswynfen6	Awikwokwikwok
7	msmietond	awjwjwjwj
8	gpierseg	aowkadsoakdo
9	tcauseyh	andjansdjasn
10	givashkovk	asdnaldnasdnakdmalkdmlaksdmal
13	tcauseyh	bismillah sukses masuk row
\.


--
-- Data for Name: pesanan_sumber_daya; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.pesanan_sumber_daya (nomor_pesanan, username_admin_satgas, total_harga) FROM stdin;
1	mteape5	5870000
2	vturbarda	4320000
3	walfonsettie	12330000
4	tweekesj	42100000
5	sdemangem	12200000
6	mteape5	5870000
7	vturbarda	4320000
8	walfonsettie	12330000
9	tweekesj	42100000
10	sdemangem	12200000
\.


--
-- Data for Name: petugas_distribusi; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.petugas_distribusi (username, no_sim) FROM stdin;
avasler0	111135247359785
jlorans1	123729568361167
wrobbeke2	137243526969250
lmcalindon3	146835247359785
awiddop4	156717171717175
fdyltfhh	166717171717175
xxarringn	137243526900250
\.


--
-- Data for Name: petugas_faskes; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.petugas_faskes (username) FROM stdin;
tswynfen6
msmietond
gpierseg
tcauseyh
givashkovk
\.


--
-- Data for Name: riwayat_status_pengiriman; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.riwayat_status_pengiriman (kode_status_batch_pengiriman, kode_batch, tanggal) FROM stdin;
PRO	BP001	2021-03-09 00:01:00
PRO	BP002	2021-03-09 00:02:00
PRO	BP003	2021-03-09 00:03:00
PRO	BP004	2021-03-09 00:04:00
PRO	BP005	2021-03-09 00:05:00
PRO	BP006	2021-03-10 00:06:00
PRO	BP007	2021-03-10 00:07:00
OTW	BP001	2021-03-15 00:08:00
OTW	BP002	2021-03-15 00:09:00
OTW	BP003	2021-03-15 00:10:00
OTW	BP004	2021-03-15 00:11:00
OTW	BP005	2021-03-15 00:12:00
OTW	BP006	2021-03-16 00:13:00
OTW	BP007	2021-03-16 00:14:00
DLV	BP001	2021-03-20 00:15:00
HLG	BP002	2021-03-20 00:16:00
RET	BP003	2021-03-20 00:17:00
DLV	BP004	2021-03-20 00:18:00
DLV	BP005	2021-03-20 00:19:00
DLV	BP006	2021-03-20 00:20:00
DLV	BP007	2021-03-20 00:21:00
\.


--
-- Data for Name: riwayat_status_permohonan; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.riwayat_status_permohonan (kode_status_permohonan, nomor_permohonan, username_admin, tanggal) FROM stdin;
REQ	1	mteape5	2021-01-05
REQ	2	vturbarda	2021-01-06
REQ	3	walfonsettie	2021-01-07
REQ	4	mteape5	2021-01-08
REQ	5	vturbarda	2021-01-09
PRO	1	walfonsettie	2021-01-10
PRO	2	mteape5	2021-01-11
PRO	3	vturbarda	2021-01-12
PRO	4	tweekesj	2021-01-13
PRO	5	sdemangem	2021-01-14
REQ	6	mteape5	2021-01-15
REQ	7	vturbarda	2021-01-16
REQ	8	walfonsettie	2021-01-17
REQ	9	mteape5	2021-01-18
REQ	10	vturbarda	2021-01-19
WAI	13	tweekesj	2021-05-20
\.


--
-- Data for Name: riwayat_status_pesanan; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.riwayat_status_pesanan (kode_status_pesanan, no_pesanan, username_supplier, tanggal) FROM stdin;
REQ-SUP	1	kmebes9	2021-02-06
REQ-SUP	2	mopenshaw7	2021-02-06
REQ-SUP	3	gritchleyb	2021-02-07
REQ-SUP	4	skitleyi	2021-02-07
REQ-SUP	5	lharringn	2021-02-08
FIN-SUP	1	kmebes9	2021-02-09
REJ-SUP	2	mopenshaw7	2021-02-11
PRO-SUP	3	gritchleyb	2021-02-15
PRO-SUP	4	skitleyi	2021-02-17
MAS-SUP	5	lharringn	2021-02-18
\.


--
-- Data for Name: status_batch_pengiriman; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.status_batch_pengiriman (kode, nama) FROM stdin;
PRO	Diproses
OTW	Dalam perjalanan
DLV	Selesai
HLG	Hilang
RET	Dikembalikan
PEN	Pending
\.


--
-- Data for Name: status_permohonan; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.status_permohonan (kode, nama) FROM stdin;
REQ	diajukan
PRO	diproses
WAI	menunggu pengadaan
FIN	selesai
REJ	ditolak
MAS	selesai dengan masalah
\.


--
-- Data for Name: status_pesanan; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.status_pesanan (kode, nama) FROM stdin;
REQ-SUP	diajukan
REJ-SUP	ditolak
PRO-SUP	diproses
FIN-SUP	selesai
MAS-SUP	selesai dengan masalah
\.


--
-- Data for Name: stok_faskes; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.stok_faskes (kode_faskes, kode_item_sumber_daya, jumlah) FROM stdin;
FAS1	IT001	25
FAS2	IT002	20
FAS3	IT003	10
FAS4	IT004	35
FAS5	IT005	5
FAS1	IT006	10
FAS2	IT007	15
FAS3	IT008	15
FAS4	IT009	20
FAS5	IT010	20
\.


--
-- Data for Name: stok_warehouse_provinsi; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.stok_warehouse_provinsi (id_lokasi_warehouse, jumlah, kode_item_sumber_daya) FROM stdin;
L0001	550	IT001
L0002	50	IT002
L0003	433	IT003
L0004	1002	IT004
L0005	630	IT007
L0006	755	IT008
L0007	20	IT009
L0008	875	IT012
L0009	330	IT013
L0010	415	IT014
\.


--
-- Data for Name: supplier; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.supplier (username, nama_organisasi) FROM stdin;
kmebes9	Bakti Utama
mopenshaw7	Kasih Bunda
gritchleyb	Kami Bisa
skitleyi	Senyum Semesta
lharringn	Suka Damai
\.


--
-- Data for Name: tipe_faskes; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.tipe_faskes (kode, nama_tipe) FROM stdin;
RUJ	Rumah sakit rujukan
IDD	Instalasi darurat terpadu
KLI	Klinik
PUS	Puskesmas
\.


--
-- Data for Name: tipe_item; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.tipe_item (kode, nama) FROM stdin;
APD	alat pelindung diri atau APD
VEN	ventilator
BED	tempat tidur
MSK	paket masker
VAK	paket vaksin
PCR	PCR test kit
RAP	rapid test kit
\.


--
-- Data for Name: transaksi_sumber_daya; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.transaksi_sumber_daya (nomor, tanggal, total_berat, total_item) FROM stdin;
1	2020-12-23	5000	147
2	2020-12-23	2500	170
3	2020-12-24	150	130
4	2020-12-25	150	144
5	2020-12-26	601	169
6	2020-12-27	100	50
7	2020-12-28	200	100
8	2020-12-29	180	158
9	2020-12-30	300	130
10	2020-12-31	50	50
13	2021-05-20	25	25
\.


--
-- Data for Name: warehouse_provinsi; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.warehouse_provinsi (id_lokasi) FROM stdin;
L0001
L0002
L0003
L0004
L0005
L0006
L0007
L0008
L0009
L0010
\.


--
-- Data for Name: warehouse_supplier; Type: TABLE DATA; Schema: sidia; Owner: db202b17
--

COPY sidia.warehouse_supplier (id_lokasi, username_supplier) FROM stdin;
L0001	gritchleyb
L0002	kmebes9
L0003	lharringn
L0004	mopenshaw7
L0005	skitleyi
\.


--
-- Name: tsd_id_seq; Type: SEQUENCE SET; Schema: sidia; Owner: db202b17
--

SELECT pg_catalog.setval('sidia.tsd_id_seq', 1, false);


--
-- Name: admin_satgas admin_satgas_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.admin_satgas
    ADD CONSTRAINT admin_satgas_pkey PRIMARY KEY (username);


--
-- Name: batch_pengiriman batch_pengiriman_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.batch_pengiriman
    ADD CONSTRAINT batch_pengiriman_pkey PRIMARY KEY (kode);


--
-- Name: daftar_item daftar_item_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.daftar_item
    ADD CONSTRAINT daftar_item_pkey PRIMARY KEY (no_transaksi_sumber_daya, no_urut);


--
-- Name: faskes faskes_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.faskes
    ADD CONSTRAINT faskes_pkey PRIMARY KEY (kode_faskes_nasional);


--
-- Name: item_sumber_daya item_sumber_daya_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.item_sumber_daya
    ADD CONSTRAINT item_sumber_daya_pkey PRIMARY KEY (kode);


--
-- Name: kendaraan kendaraan_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.kendaraan
    ADD CONSTRAINT kendaraan_pkey PRIMARY KEY (nomor);


--
-- Name: lokasi lokasi_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.lokasi
    ADD CONSTRAINT lokasi_pkey PRIMARY KEY (id);


--
-- Name: manifes_item manifes_item_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.manifes_item
    ADD CONSTRAINT manifes_item_pkey PRIMARY KEY (kode_batch_pengiriman, no_urut);


--
-- Name: pengguna pengguna_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.pengguna
    ADD CONSTRAINT pengguna_pkey PRIMARY KEY (username);


--
-- Name: permohonan_sumber_daya_faskes permohonan_sumber_daya_faskes_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.permohonan_sumber_daya_faskes
    ADD CONSTRAINT permohonan_sumber_daya_faskes_pkey PRIMARY KEY (no_transaksi_sumber_daya);


--
-- Name: pesanan_sumber_daya pesanan_sumber_daya_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.pesanan_sumber_daya
    ADD CONSTRAINT pesanan_sumber_daya_pkey PRIMARY KEY (nomor_pesanan);


--
-- Name: petugas_distribusi petugas_distribusi_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.petugas_distribusi
    ADD CONSTRAINT petugas_distribusi_pkey PRIMARY KEY (username);


--
-- Name: petugas_faskes petugas_faskes_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.petugas_faskes
    ADD CONSTRAINT petugas_faskes_pkey PRIMARY KEY (username);


--
-- Name: riwayat_status_pengiriman riwayat_status_pengiriman_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.riwayat_status_pengiriman
    ADD CONSTRAINT riwayat_status_pengiriman_pkey PRIMARY KEY (kode_status_batch_pengiriman, kode_batch);


--
-- Name: riwayat_status_permohonan riwayat_status_permohonan_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.riwayat_status_permohonan
    ADD CONSTRAINT riwayat_status_permohonan_pkey PRIMARY KEY (kode_status_permohonan, nomor_permohonan);


--
-- Name: riwayat_status_pesanan riwayat_status_pesanan_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.riwayat_status_pesanan
    ADD CONSTRAINT riwayat_status_pesanan_pkey PRIMARY KEY (kode_status_pesanan, no_pesanan);


--
-- Name: status_batch_pengiriman status_batch_pengiriman_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.status_batch_pengiriman
    ADD CONSTRAINT status_batch_pengiriman_pkey PRIMARY KEY (kode);


--
-- Name: status_permohonan status_permohonan_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.status_permohonan
    ADD CONSTRAINT status_permohonan_pkey PRIMARY KEY (kode);


--
-- Name: status_pesanan status_pesanan_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.status_pesanan
    ADD CONSTRAINT status_pesanan_pkey PRIMARY KEY (kode);


--
-- Name: stok_faskes stok_faskes_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.stok_faskes
    ADD CONSTRAINT stok_faskes_pkey PRIMARY KEY (kode_faskes, kode_item_sumber_daya);


--
-- Name: stok_warehouse_provinsi stok_warehouse_provinsi_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.stok_warehouse_provinsi
    ADD CONSTRAINT stok_warehouse_provinsi_pkey PRIMARY KEY (id_lokasi_warehouse, kode_item_sumber_daya);


--
-- Name: supplier supplier_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.supplier
    ADD CONSTRAINT supplier_pkey PRIMARY KEY (username);


--
-- Name: tipe_faskes tipe_faskes_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.tipe_faskes
    ADD CONSTRAINT tipe_faskes_pkey PRIMARY KEY (kode);


--
-- Name: tipe_item tipe_item_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.tipe_item
    ADD CONSTRAINT tipe_item_pkey PRIMARY KEY (kode);


--
-- Name: transaksi_sumber_daya transaksi_sumber_daya_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.transaksi_sumber_daya
    ADD CONSTRAINT transaksi_sumber_daya_pkey PRIMARY KEY (nomor);


--
-- Name: warehouse_provinsi warehouse_provinsi_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.warehouse_provinsi
    ADD CONSTRAINT warehouse_provinsi_pkey PRIMARY KEY (id_lokasi);


--
-- Name: warehouse_supplier warehouse_supplier_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.warehouse_supplier
    ADD CONSTRAINT warehouse_supplier_pkey PRIMARY KEY (id_lokasi);


--
-- Name: permohonan_sumber_daya_faskes trigger_2; Type: TRIGGER; Schema: sidia; Owner: db202b17
--

CREATE TRIGGER trigger_cek_stock_sumber_daya
BEFORE INSERT ON sidia.RIWAYAT_STATUS_PERMOHONAN
FOR EACH ROW
EXECUTE PROCEDURE sidia.cek_stock_sumber_daya()

CREATE TRIGGER trigger_cek_pesanan_sumber_daya
AFTER INSERT ON sidia.DAFTAR_ITEM
FOR EACH ROW
EXECUTE PROCEDURE sidia.cek_jumlah_item_pesanan_sumber_daya();

--
-- Name: batch_pengiriman trigger_4; Type: TRIGGER; Schema: sidia; Owner: db202b17
--



CREATE TRIGGER trigger_jumlah_batch
BEFORE INSERT ON sidia.BATCH_PENGIRIMAN FOR EACH ROW
EXECUTE PROCEDURE sidia.cek_jumlah_batch();

--
-- Name: batch_pengiriman trigger_5; Type: TRIGGER; Schema: sidia; Owner: db202b17
--

CREATE TRIGGER trigger_cek_jumlah_batch
AFTER INSERT ON sidia.BATCH_PENGIRIMAN
FOR EACH ROW
EXECUTE PROCEDURE sidia.cek_jumlah_batch();

--
-- Name: pengguna username_violation; Type: TRIGGER; Schema: sidia; Owner: db202b17
--


CREATE TRIGGER trigger_cek_username
BEFORE INSERT ON sidia.PENGGUNA
FOR EACH ROW
EXECUTE PROCEDURE sidia.cek_username();

--
-- Name: admin_satgas admin_satgas_username_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.admin_satgas
    ADD CONSTRAINT admin_satgas_username_fkey FOREIGN KEY (username) REFERENCES sidia.pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: batch_pengiriman batch_pengiriman_id_lokasi_asal_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.batch_pengiriman
    ADD CONSTRAINT batch_pengiriman_id_lokasi_asal_fkey FOREIGN KEY (id_lokasi_asal) REFERENCES sidia.lokasi(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: batch_pengiriman batch_pengiriman_id_lokasi_tujuan_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.batch_pengiriman
    ADD CONSTRAINT batch_pengiriman_id_lokasi_tujuan_fkey FOREIGN KEY (id_lokasi_tujuan) REFERENCES sidia.lokasi(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: batch_pengiriman batch_pengiriman_no_kendaraan_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.batch_pengiriman
    ADD CONSTRAINT batch_pengiriman_no_kendaraan_fkey FOREIGN KEY (no_kendaraan) REFERENCES sidia.kendaraan(nomor) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: batch_pengiriman batch_pengiriman_nomor_transaksi_sumber_daya_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.batch_pengiriman
    ADD CONSTRAINT batch_pengiriman_nomor_transaksi_sumber_daya_fkey FOREIGN KEY (nomor_transaksi_sumber_daya) REFERENCES sidia.transaksi_sumber_daya(nomor) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: batch_pengiriman batch_pengiriman_username_petugas_distribusi_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.batch_pengiriman
    ADD CONSTRAINT batch_pengiriman_username_petugas_distribusi_fkey FOREIGN KEY (username_petugas_distribusi) REFERENCES sidia.petugas_distribusi(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: batch_pengiriman batch_pengiriman_username_satgas_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.batch_pengiriman
    ADD CONSTRAINT batch_pengiriman_username_satgas_fkey FOREIGN KEY (username_satgas) REFERENCES sidia.admin_satgas(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: daftar_item daftar_item_kode_item_sumber_daya_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.daftar_item
    ADD CONSTRAINT daftar_item_kode_item_sumber_daya_fkey FOREIGN KEY (kode_item_sumber_daya) REFERENCES sidia.item_sumber_daya(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: daftar_item daftar_item_no_transaksi_sumber_daya_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.daftar_item
    ADD CONSTRAINT daftar_item_no_transaksi_sumber_daya_fkey FOREIGN KEY (no_transaksi_sumber_daya) REFERENCES sidia.transaksi_sumber_daya(nomor) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: faskes faskes_id_lokasi_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.faskes
    ADD CONSTRAINT faskes_id_lokasi_fkey FOREIGN KEY (id_lokasi) REFERENCES sidia.lokasi(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: faskes faskes_kode_tipe_faskes_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.faskes
    ADD CONSTRAINT faskes_kode_tipe_faskes_fkey FOREIGN KEY (kode_tipe_faskes) REFERENCES sidia.tipe_faskes(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: faskes faskes_username_petugas_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.faskes
    ADD CONSTRAINT faskes_username_petugas_fkey FOREIGN KEY (username_petugas) REFERENCES sidia.petugas_faskes(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: item_sumber_daya item_sumber_daya_kode_tipe_item_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.item_sumber_daya
    ADD CONSTRAINT item_sumber_daya_kode_tipe_item_fkey FOREIGN KEY (kode_tipe_item) REFERENCES sidia.tipe_item(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: item_sumber_daya item_sumber_daya_username_supplier_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.item_sumber_daya
    ADD CONSTRAINT item_sumber_daya_username_supplier_fkey FOREIGN KEY (username_supplier) REFERENCES sidia.supplier(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: manifes_item manifes_item_kode_batch_pengiriman_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.manifes_item
    ADD CONSTRAINT manifes_item_kode_batch_pengiriman_fkey FOREIGN KEY (kode_batch_pengiriman) REFERENCES sidia.batch_pengiriman(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: manifes_item manifes_item_kode_item_sumber_daya_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.manifes_item
    ADD CONSTRAINT manifes_item_kode_item_sumber_daya_fkey FOREIGN KEY (kode_item_sumber_daya) REFERENCES sidia.item_sumber_daya(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: permohonan_sumber_daya_faskes permohonan_sumber_daya_faskes_no_transaksi_sumber_daya_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.permohonan_sumber_daya_faskes
    ADD CONSTRAINT permohonan_sumber_daya_faskes_no_transaksi_sumber_daya_fkey FOREIGN KEY (no_transaksi_sumber_daya) REFERENCES sidia.transaksi_sumber_daya(nomor) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: permohonan_sumber_daya_faskes permohonan_sumber_daya_faskes_username_petugas_faskes_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.permohonan_sumber_daya_faskes
    ADD CONSTRAINT permohonan_sumber_daya_faskes_username_petugas_faskes_fkey FOREIGN KEY (username_petugas_faskes) REFERENCES sidia.petugas_faskes(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pesanan_sumber_daya pesanan_sumber_daya_nomor_pesanan_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.pesanan_sumber_daya
    ADD CONSTRAINT pesanan_sumber_daya_nomor_pesanan_fkey FOREIGN KEY (nomor_pesanan) REFERENCES sidia.transaksi_sumber_daya(nomor) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pesanan_sumber_daya pesanan_sumber_daya_username_admin_satgas_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.pesanan_sumber_daya
    ADD CONSTRAINT pesanan_sumber_daya_username_admin_satgas_fkey FOREIGN KEY (username_admin_satgas) REFERENCES sidia.admin_satgas(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: petugas_distribusi petugas_distribusi_username_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.petugas_distribusi
    ADD CONSTRAINT petugas_distribusi_username_fkey FOREIGN KEY (username) REFERENCES sidia.pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: petugas_faskes petugas_faskes_username_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.petugas_faskes
    ADD CONSTRAINT petugas_faskes_username_fkey FOREIGN KEY (username) REFERENCES sidia.pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: riwayat_status_pengiriman riwayat_status_pengiriman_kode_batch_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.riwayat_status_pengiriman
    ADD CONSTRAINT riwayat_status_pengiriman_kode_batch_fkey FOREIGN KEY (kode_batch) REFERENCES sidia.batch_pengiriman(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: riwayat_status_pengiriman riwayat_status_pengiriman_kode_status_batch_pengiriman_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.riwayat_status_pengiriman
    ADD CONSTRAINT riwayat_status_pengiriman_kode_status_batch_pengiriman_fkey FOREIGN KEY (kode_status_batch_pengiriman) REFERENCES sidia.status_batch_pengiriman(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: riwayat_status_permohonan riwayat_status_permohonan_kode_status_permohonan_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.riwayat_status_permohonan
    ADD CONSTRAINT riwayat_status_permohonan_kode_status_permohonan_fkey FOREIGN KEY (kode_status_permohonan) REFERENCES sidia.status_permohonan(kode);


--
-- Name: riwayat_status_permohonan riwayat_status_permohonan_nomor_permohonan_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.riwayat_status_permohonan
    ADD CONSTRAINT riwayat_status_permohonan_nomor_permohonan_fkey FOREIGN KEY (nomor_permohonan) REFERENCES sidia.permohonan_sumber_daya_faskes(no_transaksi_sumber_daya) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: riwayat_status_permohonan riwayat_status_permohonan_username_admin_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.riwayat_status_permohonan
    ADD CONSTRAINT riwayat_status_permohonan_username_admin_fkey FOREIGN KEY (username_admin) REFERENCES sidia.admin_satgas(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: riwayat_status_pesanan riwayat_status_pesanan_kode_status_pesanan_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.riwayat_status_pesanan
    ADD CONSTRAINT riwayat_status_pesanan_kode_status_pesanan_fkey FOREIGN KEY (kode_status_pesanan) REFERENCES sidia.status_pesanan(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: riwayat_status_pesanan riwayat_status_pesanan_no_pesanan_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.riwayat_status_pesanan
    ADD CONSTRAINT riwayat_status_pesanan_no_pesanan_fkey FOREIGN KEY (no_pesanan) REFERENCES sidia.pesanan_sumber_daya(nomor_pesanan) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: riwayat_status_pesanan riwayat_status_pesanan_username_supplier_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.riwayat_status_pesanan
    ADD CONSTRAINT riwayat_status_pesanan_username_supplier_fkey FOREIGN KEY (username_supplier) REFERENCES sidia.supplier(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: stok_faskes stok_faskes_kode_faskes_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.stok_faskes
    ADD CONSTRAINT stok_faskes_kode_faskes_fkey FOREIGN KEY (kode_faskes) REFERENCES sidia.faskes(kode_faskes_nasional) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: stok_faskes stok_faskes_kode_item_sumber_daya_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.stok_faskes
    ADD CONSTRAINT stok_faskes_kode_item_sumber_daya_fkey FOREIGN KEY (kode_item_sumber_daya) REFERENCES sidia.item_sumber_daya(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: stok_warehouse_provinsi stok_warehouse_provinsi_id_lokasi_warehouse_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.stok_warehouse_provinsi
    ADD CONSTRAINT stok_warehouse_provinsi_id_lokasi_warehouse_fkey FOREIGN KEY (id_lokasi_warehouse) REFERENCES sidia.warehouse_provinsi(id_lokasi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: stok_warehouse_provinsi stok_warehouse_provinsi_kode_item_sumber_daya_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.stok_warehouse_provinsi
    ADD CONSTRAINT stok_warehouse_provinsi_kode_item_sumber_daya_fkey FOREIGN KEY (kode_item_sumber_daya) REFERENCES sidia.item_sumber_daya(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: supplier supplier_username_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.supplier
    ADD CONSTRAINT supplier_username_fkey FOREIGN KEY (username) REFERENCES sidia.pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: warehouse_provinsi warehouse_provinsi_id_lokasi_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.warehouse_provinsi
    ADD CONSTRAINT warehouse_provinsi_id_lokasi_fkey FOREIGN KEY (id_lokasi) REFERENCES sidia.lokasi(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: warehouse_supplier warehouse_supplier_id_lokasi_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.warehouse_supplier
    ADD CONSTRAINT warehouse_supplier_id_lokasi_fkey FOREIGN KEY (id_lokasi) REFERENCES sidia.lokasi(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: warehouse_supplier warehouse_supplier_username_supplier_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202b17
--

ALTER TABLE ONLY sidia.warehouse_supplier
    ADD CONSTRAINT warehouse_supplier_username_supplier_fkey FOREIGN KEY (username_supplier) REFERENCES sidia.supplier(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--